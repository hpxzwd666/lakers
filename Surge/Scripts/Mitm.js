!(async () => {
let mods = (await httpAPI("GET", "/v1/modules", null))
let modsStatus = /❏ MitM All/.test(mods.enabled)
let capture = (await httpAPI("GET", "/v1/features/capture", null))
if ($trigger === "button") {
    if (capture.enabled == false){
        await httpAPI("POST", "/v1/features/capture", {enabled: "true"})
        await httpAPI("POST", "/v1/modules", {"❏ MitM All": "true"})
        $done({
            title:"HTTP Capture",
            content:"全局抓包 → 启动",
            icon: "externaldrive.badge.checkmark",
            "icon-color": "63c936"
        })
    } else {
        await httpAPI("POST", "/v1/features/capture", {enabled: "false"})
        await httpAPI("POST", "/v1/modules", {"❏ MitM All": "false"})
        $done({
            title:"HTTP Capture",
            content:"全局抓包 → 关闭",
            icon: "externaldrive.badge.xmark",
            "icon-color": "#f20e00"
        })
    }
} else if(modsStatus == true || capture.enabled == true) {
        $done({
            title:"http capture",
            content:"capture: " + iconStatus(capture.enabled) + "     hostnames: " + iconStatus(modsStatus),
            icon: "touchid",
            "icon-color": "ED0121"
        })
} else {
        $done({
            title:"http capture",
            content:"capture: " + iconStatus(capture.enabled) + "     hostnames: " + iconStatus(modsStatus),
            icon: "touchid",
            "icon-color": "F22e00"
        })
}
})();

function httpAPI(method = "", path = "", body = "") {
    return new Promise((resolve) => {
        $httpAPI(method, path, body, (result) => {
            resolve(result);
        });
    });
}

function iconStatus(status) {
  if (status) {
    return "\u2611";
  } else {
    return "\u2612"
  }
}